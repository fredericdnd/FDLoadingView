import UIKit
import FDLoadingView

class ViewController: UIViewController {

    // Properties
    let loadingView = FDLoadingView(title: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func showLoading(_ sender: Any) {
        loadingView.show()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            self.loadingView.hide(completionHandler: nil)
        }
    }
    
}

