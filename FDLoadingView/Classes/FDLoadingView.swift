import UIKit
import NVActivityIndicatorView

open class FDLoadingView: UIView {
    
    private var title: String?
    private let indicatorSize = CGSize(width: 26.0, height: 26.0)
    private var activityIndicator: NVActivityIndicatorView!
    
    open var animated: Bool = true
    open var cornerRadius: CGFloat = 8.0
    open var alertWidth: CGFloat?
    open var padding: CGFloat = 16.0
    open var textColor = UIColor(red: 48/255, green: 48/255, blue: 48/255, alpha: 1.0)
    open var indicatorColor = UIColor.black
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var alertWidthConstraint: NSLayoutConstraint!
    
    public convenience init(title: String?) {
        self.init(frame: UIScreen.main.bounds)
        
        self.title = title
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    private func loadViewFromNib() -> UIView {
        let viewBundle = Bundle(for: type(of: self))
        let view = viewBundle.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)![0]
        
        return view as! UIView
    }
    
    private func setupView() {
        let nibView = loadViewFromNib()
        nibView.frame = bounds
        nibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(nibView)
        
        containerView.layer.cornerRadius = cornerRadius
        stackView.spacing = padding
        stackView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    internal func configure() {
        if let title = title {
            addTitleToAlert(title: title)
            alertWidth = UIScreen.main.bounds.width - padding * 2
        }
        
        if let alertWidth = alertWidth {
            alertWidthConstraint.constant = alertWidth
        } else {
            alertWidthConstraint.constant = indicatorSize.width + 32.0
        }
        
        containerView.layoutIfNeeded()
        
        addActivityIndicator()
    }
    
    private func addActivityIndicator() {
        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0.0, y: 0.0, width: indicatorSize.width, height: indicatorSize.height), type: .circleStrokeSpin, color: indicatorColor)
        activityIndicator.widthAnchor.constraint(greaterThanOrEqualToConstant: 26.0).isActive = true
        activityIndicator.heightAnchor.constraint(greaterThanOrEqualToConstant: 26.0).isActive = true
        activityIndicator.startAnimating()
        stackView.addArrangedSubview(activityIndicator)
    }
    
    private func addTitleToAlert(title: String) {
        let titleLabel = UILabel()
        titleLabel.text = title
        titleLabel.textAlignment = .center
        titleLabel.textColor = textColor
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont.systemFont(ofSize: 17.0, weight: .semibold)
        titleLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 0.0).isActive = true
        stackView.addArrangedSubview(titleLabel)
        
        titleLabel.layoutIfNeeded()
    }
    
}
