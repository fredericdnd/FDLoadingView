# FDLoadingView

[![CI Status](https://img.shields.io/travis/frederic.dinand.developer@gmail.com/FDLoadingView.svg?style=flat)](https://travis-ci.org/frederic.dinand.developer@gmail.com/FDLoadingView)
[![Version](https://img.shields.io/cocoapods/v/FDLoadingView.svg?style=flat)](https://cocoapods.org/pods/FDLoadingView)
[![License](https://img.shields.io/cocoapods/l/FDLoadingView.svg?style=flat)](https://cocoapods.org/pods/FDLoadingView)
[![Platform](https://img.shields.io/cocoapods/p/FDLoadingView.svg?style=flat)](https://cocoapods.org/pods/FDLoadingView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FDLoadingView is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FDLoadingView'
```

## Author

frederic.dinand.developer@gmail.com, frederic.dinand.developer@gmail.com

## License

FDLoadingView is available under the MIT license. See the LICENSE file for more info.
